let mysql = require("mysql");

//modify the following credential to connect to local database
let con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "rootpassword",
  database: "taxitrips"
});

//connect to local mysql database
con.connect(err => {
  if (err) throw err;
  console.log("connected to mysql");
});

//export the connection to be used in other modules
module.exports = con;
