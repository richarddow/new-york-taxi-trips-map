let express = require("express");
let router = express.Router();
let database = require("../database");

router.get("/trips", (req, res) => {
  database.query("SELECT * FROM NEW_YORK_TRIPS;", (err, results, fields) => {
    if (err) throw err;
    //console.log("Results: " + results);
    res.send(results);
  });
});

router.get("/boroughs", (req, res) => {
  database.query("SELECT * FROM BOROUGH;", (err, results, fields) => {
    if (err) throw err;
    // console.log("Boroughs: " + results);
    res.send(results);
  });
});

router.get("/trips/:borough", (req, res) => {
  database.query(
    `SELECT * FROM NEW_YORK_TRIPS WHERE name = "${req.params.borough}"`,
    (err, results, fields) => {
      if (err) throw err;
      res.send(results);
    }
  );
});

module.exports = router;
