let express = require("express");
let app = express();
let tripRoute = require("./routes/trip");
let bodyParser = require("body-parser");

app.use(bodyParser.json());

app.use(tripRoute);

// app.use(express.static("public"));

//not found error at the end
app.use((req, res, next) => {
  res.status(404).send("not found");
});


//listen to port 3000
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log(`server has started on ${PORT}`));
