This project is a website that allows you to see how many taxi trips are there in the month of January 2015 in US.

![alt text](showcase.png)

## How to Get It Running

To install all dependencies, run:

### `yarn install`

To start the app, run:

### `yarn start`

Open [http://localhost:3001](http://localhost:3001) to view it in the browser.

## To Get The Database Up and Running

1. Install MySQL
2. Create a 'root' user with password as 'rootpassword' (alternatively alter the credential details found in ./server/database.js)
3. Create a database named 'taxitrips'
4. Import boroughs.csv and new_york_trip.csv into 'taxitrips'
