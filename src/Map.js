import React, {Component} from 'react';
import {StaticMap} from 'react-map-gl';
import DeckGL, {ScatterplotLayer} from 'deck.gl';
import axios from 'axios'

const INITIAL_VIEWSTATE = {
  longitude: -74,
  latitude: 40.7,
  zoom: 11,
  maxZoom: 11,
  // minZoom:11,
  pitch: 0,
  bearing: 0
};

export class Map extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      viewState: INITIAL_VIEWSTATE
    }
    this._onViewStateChange = this._onViewStateChange.bind(this)
  }

  //render the tooltip that shows the coordinate when hovering 
  _renderTooltip() {
    //pick out the hoverobj,pX and pY from the state
    const {hoveredObject, pointerX, pointerY} = this.state || {};
    //check if hoveredobj is available, if yes, render the tooltip
    return hoveredObject && (
      <div style={{position: 'absolute', zIndex: 3, pointerEvents: 'none', left: pointerX, top: pointerY}}>
        { hoveredObject.pickup_longitude }, { hoveredObject.pickup_latitude }
      </div>
    );
  }

  //render the scatter plot layer
  _renderLayers() {
    return [new ScatterplotLayer({
        id: 'scatter-plot',
        data: this.state.data,
        pickable: true,
        radiusScale: 10,
        radiusMinPixels: 0.25,
        getPosition: d => [d.pickup_longitude, d.pickup_latitude, 0],
        getFillColor: [0, 128, 255],
        getRadius: 1,
        onHover: info => this.setState({
          hoveredObject: info.object,
          pointerX: info.x,
          pointerY: info.y
        })
      })]
    
  }

  //used to set the viewport so that the map is draggable
  _onViewStateChange({viewState}) {
    //console.log(viewState);
    this.setState({viewState});

  }

  //load the data when the component mounts
  componentDidMount() {
    console.log('fetch data')
    axios
      .get("/trips")
      .then(res =>{
        console.log('http response received')
        // console.log(res.data)
        this.setState({data: res.data})
        console.log(this.state.data)
      })
  }

  //fetch the datapoints for the selected borough
  componentDidUpdate(prevProps) {
    // console.log('fetch borough data')
    (prevProps.bor !== this.props.bor) && axios
      .get(`/trips/${this.props.bor}`)
      .then(res =>{
        console.log('http response received')
        // console.log(res.data)
        this.setState({data: res.data})
        console.log(this.state.data)
      })
  }

  render() {
    const {mapStyle = 'mapbox://styles/mapbox/light-v9'} = this.props;
    console.log(this.props.bor)
    return (
        <DeckGL layers={this._renderLayers()} width="50vw" viewState={this.state.viewState} 
        onViewStateChange={this._onViewStateChange} onViewPortChange={(viewport) => console.log(viewport)} 
        controller={true}>
            <StaticMap
                reuseMaps
                mapStyle={mapStyle}
                preventStyleDiffing={true}
                mapboxApiAccessToken={process.env.REACT_APP_MAPBOX_TOKEN}
            />    
        { this._renderTooltip() }
        </DeckGL>
      
    );
  }
}

export default Map;