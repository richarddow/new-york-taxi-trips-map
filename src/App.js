import React, {useState, useEffect, useRef} from 'react';
import Map from './Map';
import axios from 'axios';
import './App.css';


function App () {

  //all states related to boroughs, an additional layer of authentication and
  //identification is required to know which borough the user is from.
  //Right now, there would be a selector for choosing and fetching boroughs as
  //a proof of concept
  const [boroughs, setBoroughs] = useState([])
  const [borComps, setBorComps] = useState([])
  const [selectedBor, setSelectedBor] = useState('')
  const borSelector = useRef(null)

  //could use a reducer to make this part more readable and manageable 
  //all states related to filtering date and time. This is currently not
  //fully implemented 
  const startDateRef = useRef(null)
  const endDateRef = useRef(null)
  const startTimeRef = useRef(null)
  const endTimeRef = useRef(null)
  const [startDateRange, setStartDateRange] = useState([])
  const [endDateRange, setEndDateRange] = useState([])
  const [startTimeRange, setStartTimeRange] = useState([])
  const [endTimeRange, setEndTimeRange] = useState([])
  const [startDate, setStartDate] = useState(0)
  const [endDate, setEndDate] = useState(31)
  const [startTime, setStartTime] = useState(0)
  const [endTime, setEndTime] = useState(24)

  //fetch the boroughs data
  //will not be working unless there is a database set up
  useEffect(() => {
    axios
        .get("/boroughs")
        .then(res => {
          console.log('boroughs received')
          setBoroughs(res.data)
        })
  }, [])

  //create components for borough selections
  useEffect(() => {
    let borComponents = []
    for (let i = 0; i < boroughs.length; i++) {
      borComponents.push(<option value={boroughs[i].name} onClick={() => setSelectedBor(borSelector.current.value)}>{boroughs[i].name}</option>)
    }
    setBorComps(borComponents)
  }, [boroughs])

  //calculate the date range that needs to be displayed before the selected end date
  useEffect(() => {
    let range = []
    for (let i = 1; i < (endDate + 1); i++) {
      range.push(<option value={i} onClick={() => setStartDate(parseInt(startDateRef.current.value))}>{i < 10? '0' + i.toString(): i}/01/2015</option>)
    }
    for (let i = endDate + 1; i < 32; i++) {
      range.push(<option disabled value={i}>{i < 10? '0' + i.toString(): i}/01/2015</option>)
    }
    setStartDateRange(range)
  }, [endDate])

  //calculate the date range that needs to be displayed after the selected start date
  useEffect(() => {
    let range = []
    for (let i = 1; i < startDate; i++) {
      range.push(<option disabled value={i}>{i < 10? '0' + i.toString(): i}/01/2015</option>)
    }
    for (let i = startDate; i < 32; i++) {
      range.push(<option value={i} onClick={() => setEndDate(parseInt(endDateRef.current.value))}>{i < 10? '0' + i.toString(): i}/01/2015</option>)
    }
    
    setEndDateRange(range)
  }, [startDate])

  //calculate the time range that needs to be displayed before the selected end time
  useEffect(() => {
    let range = []
    for (let t = 0; t < endTime; t++) {
      range.push(<option value={t} onClick={() => setStartTime(parseInt(startTimeRef.current.value))}>{t < 12 ? t.toString() + "AM" : (t === 12? (t).toString() + "PM" : (t - 12).toString() + "PM")}</option>)
    }
    for (let t = endTime; t < 24; t++) {
      range.push(<option disabled value={t}>{t < 12 ? t.toString() + "AM" : (t === 12? (t).toString() + "PM" : (t - 12).toString() + "PM")}</option>)
    }
    setStartTimeRange(range)
  }, [endTime])

  //calculate the time range that needs to be displayed after the selected start time
  useEffect(() => {
    let range = []
    for (let t = 1; t < startTime + 1; t++) {
      range.push(<option disabled value={t} >{t < 12 ? t.toString() + "AM" : (t === 12? (t).toString() + "PM" : (t - 12).toString() + "PM")}</option>)
    }
    for (let t = startTime + 1; t < 24; t++) {
      range.push(<option value={t} onClick={() => setEndTime(parseInt(endTimeRef.current.value))}>{t < 12 ? t.toString() + "AM" : (t === 12? (t).toString() + "PM" : (t - 12).toString() + "PM")}</option>)
    }
    
    setEndTimeRange(range)
  }, [startTime])


  return (
    <div className='container'>
      {console.log('rerender')}
      <Map className='map' bor={selectedBor}/>
      <p>FROM</p>
      <select name="Start Date" ref={startDateRef}>
        <option value="" disabled selected>Choose Start Date</option>
        {startDateRange}
      </select>
      <p>TO</p>
      <select name="End Date" ref={endDateRef}>
        <option value="" disabled selected>Choose End Date</option>
        {endDateRange}
      </select> 
      <div></div>
      <p>FROM</p>
      <select name="Start Time" ref={startTimeRef}>
        <option value="" disabled selected>Choose Start Time</option>
        {startTimeRange}
      </select> 
      <p>TO</p>
      <select name="End Time" ref={endTimeRef}>
        <option value="" disabled selected>Choose End Time </option>
        {endTimeRange}
      </select> 
      <div></div>
      <p>BOROUGHS:</p>
      <select name="Start Time" ref={borSelector}>
        <option value="" disabled selected>Choose Boroughs</option>
        {borComps}
      </select> 
    </div>
    
  )
}

export default App;